import androidx.compose.ui.graphics.Color

val red1 = Color(0.9f, 0.1f, 0.2f)
val red2 = Color(0.8f, 0.2f, 0.1f)
val red3 = Color(0.7f, 0.1f, 0.2f)
val red4 = Color(0.9f, 0.2f, 0.1f)
val red5 = Color(0.6f, 0.1f, 0.2f)
val red6 = Color(0.5f, 0.2f, 0.1f)
val red7 = Color(0.5f, 0.2f, 0.2f)
val red8 = Color(0.7f, 0.1f, 0.1f)

fun createColoursList(allReds: Boolean = false) = if (allReds) listOf(
    listOf(red1, red2, red3, red4, Color.Red),
    listOf(Color.Red, Color.Gray, red7, red8, red4),
    listOf(Color.Red, Color.Gray, Color.Red, Color.Red, Color.Gray, Color.LightGray),
    listOf(Color.DarkGray, red5, red6, Color.Red, red2),
    listOf(Color.DarkGray, red1, red3, red5, Color.Red),
    listOf(red7, Color.Red, Color.DarkGray, Color.Red, red8)
) else listOf(
    listOf(Color.Cyan, Color.Green, Color.Yellow, Color.Green, Color.Cyan),
    listOf(Color.Red, Color.Gray, Color.Magenta, Color.Yellow, Color.Blue),
    listOf(Color.Red, Color.Gray, Color.Red, Color.Red, Color.Gray, Color.LightGray),
    listOf(Color.DarkGray, Color.Black, Color.Blue, Color.Red, Color.Green),
    listOf(Color.DarkGray, Color.Blue, Color.Black, Color.Blue, Color.Red),
    listOf(Color.DarkGray, Color.Red, Color.DarkGray, Color.Red, Color.DarkGray)
)
