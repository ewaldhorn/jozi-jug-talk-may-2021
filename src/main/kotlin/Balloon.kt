import androidx.compose.ui.graphics.Color

data class Balloon(
    var xPos: Float,
    var xBool: Boolean,
    var xSpeed: Int,
    var yPos: Float,
    var yBool: Boolean,
    var ySpeed: Int,
    val colors: List<Color>
) {
    fun update(minX: Float, maxX: Float, minY: Float, maxY: Float) {
        with(applyMaxMin(minX, maxX, xBool, xPos, xSpeed)) {
            xPos = this.first
            xBool = this.second
        }
        with(applyMaxMin(minY, maxY, yBool, yPos, ySpeed)) {
            yPos = this.first
            yBool = this.second
        }
    }

    private fun applyMaxMin(
        minVal: Float,
        maxVal: Float,
        boolean: Boolean,
        pos: Float,
        speed: Int
    ): Pair<Float, Boolean> {
        var tmpPos = pos
        var tmpBool = boolean

        when (boolean) {
            true -> tmpPos += speed
            false -> tmpPos -= speed
        }

        if (tmpPos > maxVal) {
            tmpPos = maxVal
            tmpBool = false
        }

        if (tmpPos < minVal) {
            tmpPos = minVal
            tmpBool = true
        }

        return Pair(tmpPos, tmpBool)
    }
}
