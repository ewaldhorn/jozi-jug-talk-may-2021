import androidx.compose.runtime.*

@Composable
fun tickerMillis(): State<Long> {
    val millis = mutableStateOf(0L)

    LaunchedEffect(Unit) {
        while (true) {
            withFrameMillis {
                millis.value = it
            }
        }
    }

    return millis
}
