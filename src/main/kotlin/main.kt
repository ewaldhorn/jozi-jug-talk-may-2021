import androidx.compose.desktop.Window
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import kotlin.random.Random

/**
 * Worst practice reference code.
 *
 * This is a great example of how not to do things, unless you are just messing around and learning, of course.
 */
fun main() = Window(
    title = "Ninety Nine or Less Balloons",
    size = IntSize(800, 600),
    resizable = false
) {
    val balloons = buildBalloons(10)
    val ticker = tickerMillis()

    Surface(
        Modifier.fillMaxSize(),
        border = BorderStroke(width = 2.dp, color = Color.DarkGray),
        color = Color(red = 40, green = 40, blue = 40)
    ) {
        drawBalloons(balloons, ticker.value)
    }
}

@Composable
fun drawBalloons(balloons: List<Balloon>, tickerValue: Long) =
    Canvas(Modifier.fillMaxSize()) {
        balloons.forEach { balloon ->
            balloon.update(100f, 1500f, 100f, 1050f)
            drawBalloon(Offset(balloon.xPos, balloon.yPos), balloon.colors, this)
        }
    }

fun drawBalloon(offset: Offset, colors: List<Color>, drawScope: DrawScope) {
    val gradient = Brush.linearGradient(colors = colors)
    drawScope.drawCircle(gradient, radius = 60f, center = offset)
}

fun buildBalloons(count: Int): List<Balloon> {
    val tmpList = mutableListOf<Balloon>()

    val colorList = createColoursList()

    for (i in 1..count) {
        tmpList.add(
            Balloon(
                1000 * Random.nextFloat(),
                Random.nextBoolean(),
                Random.nextInt(1, 7),
                1000 * Random.nextFloat(),
                Random.nextBoolean(),
                Random.nextInt(1, 7),
                colorList[Random.nextInt(0, colorList.size)],
            )
        )
    }

    return tmpList.toList()
}
