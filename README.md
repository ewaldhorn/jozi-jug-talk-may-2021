# Compose for Desktop Bouncing Balloons

This is the source code for my talk(s) around Kotlin and Compose on the Desktop.

## Assumptions

With this talk, and example, I will assume that you have some basic [Kotlin][kotlinlang] experience and that you have
*completed* the [official Compose tutorial][tutorial] already.  You will get the most from the talk and this example if
you have implemented the tutorial code and have a vague grasp of what Compose does and how it works.

## Building and Running

Check out the repo to your local machine. Ensure that you have Java available.  In my case, I used Java 16,
but I have had reports of people using 11 and 14 successfully.  

The project uses the [Gradle][gradle] build tool to manage dependencies as well as the build process.

You can run the code from any IDE that supports Gradle with the `run` task.  With the first run, expect a bit of a delay
as Gradle downloads all the dependencies first.

## Learning more

You can also find a [recording of this talk][recording] on YouTube and I suggest you watch that to get a more detailed 
explanation of what this is all about.


## Resources

Shameless plug : My other talks can be found in the About section on [my website][nofuss].

JetBrains has some fantastic [official documentation][docs] available on their website to get your started with Compose.
Included in these docs are [tutorials][tutorial] that take you through getting Compose configured and up and running on 
your system.

[recording]: https://www.youtube.com/watch?v=bPCjaKcgfBk
[nofuss]: https://www.nofuss.co.za/about/
[gradle]: https://gradle.org/
[docs]: https://www.jetbrains.com/lp/compose/
[tutorial]: https://github.com/JetBrains/compose-jb/blob/master/tutorials/Getting_Started/README.md
[kotlinlang]: https://kotlinlang.org/
